package com.mengual.prog.A8;

public class Matriu {

    private int[][] matriu;

    public Matriu(int files, int columnes) {

        this.matriu = new int[files][columnes];

    }

    public void omplirMatriu(){

        for (int i = 0; i < (matriu.length); i++){

            for (int x = 0; x < (matriu[0].length); x++){

                matriu [i][x] = (int) (Math.random() * 10);

            }

        }

    }

    public void visualitzarMatriu(){

        for (int i = 0; i < (matriu.length); i++){

            for (int x = 0; x < (matriu[0].length); x++){

                System.out.print(matriu[i][x]);

            }

            System.out.print("\n");

        }

    }

    public int[][] crearMatriu7x7(){

        int[][] matriz = new int[7][7];

        return matriz;

    }

}
