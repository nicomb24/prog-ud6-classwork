package com.mengual.prog.A12;

public class Activitat12 {

    private String cadena;

    public Activitat12(String cadena) {
        this.cadena = cadena;
    }


    public int contarOFinal(){

        String[] array = cadena.split(" ");

        int total = 0;

        for (int i = 0; i < array.length; i++){

            if (array[i].matches(".*o$")){
                total++;
            }

        }

        return total;

    }


}
