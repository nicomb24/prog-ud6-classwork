package com.mengual.prog.A17;

public class Activitat17 {

    private String cadena;

    public Activitat17(String cadena) {

        this.cadena = cadena;
    }

    public boolean esValidTel(){

        return cadena.matches("^((00|\\+)?34)?[9][0-9]{8}$");

    }

}
