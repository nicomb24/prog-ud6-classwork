package com.mengual.prog.A3;

import java.util.Scanner;

public class ArrayA3 {

    private int[] array;

    private Scanner lectorTeclat;


    public ArrayA3(int[] array) {
        this.array = array;
        this.lectorTeclat = new Scanner(System.in);
    }

    public void carregaArray(){

        for (int i = 0; i < array.length; i++){

            System.out.print("Introduce un número \n");

            array[i] = lectorTeclat.nextInt();

        }

    }

    public void llegeixArray(){
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
        System.out.print("\n");
    }

}
