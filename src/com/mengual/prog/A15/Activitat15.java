package com.mengual.prog.A15;

public class Activitat15 {

    private String cadena;

    public Activitat15(String cadena) {

        this.cadena = cadena;
    }

    public boolean esValidoDni(){

        if (cadena.matches("[0-9]{8}[A-H J-N P Q S T W-Z]")){
            return true;
        } else {
            return false;
        }
    }

}
