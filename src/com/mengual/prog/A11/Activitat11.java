package com.mengual.prog.A11;

public class Activitat11 {

    private String cadena;

    public Activitat11(String cadena) {
        this.cadena = cadena;
    }

    public String substituirNum(){

        String cadena2 = cadena.replaceAll("[0-9]+","*");

        return cadena2;

    }

}
