package com.mengual.prog.A14;

public class Activitat14 {

    private String cadena;

    public Activitat14(String cadena) {

        this.cadena = cadena;

    }

    public boolean esValid(){

        if (cadena.matches("[0-9]{4}-[0-9]{4}-[0-9]{2}-[0-9]{10}")){
            return true;
        } else {
            return false;
        }

    }

}
