package com.mengual.prog.A16;

public class Activitat16 {

    private String cadena;

    public Activitat16(String cadena) {

        this.cadena = cadena;
    }

    public boolean esValidTel(){

        return cadena.matches("^(00|\\+)?34[67][0-9]{8}$");

    }

}
