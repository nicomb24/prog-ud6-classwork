package com.mengual.prog.A16;

import com.mengual.prog.A1.Activitat1;

public class Main {

    public static void main(String [] args){

        Activitat16 num = new Activitat16("0034669532726");
        Activitat16 num2 = new Activitat16("+34669532726");
        Activitat16 num3 = new Activitat16("34669532726");
        Activitat16 num4 = new Activitat16("0035669532726");
        Activitat16 num5 = new Activitat16("0034869532726");

        System.out.print(num.esValidTel());
        System.out.print(num2.esValidTel());
        System.out.print(num3.esValidTel());
        System.out.print(num4.esValidTel());
        System.out.print(num5.esValidTel());

    }

}
