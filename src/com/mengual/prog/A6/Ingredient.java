package com.mengual.prog.A6;

public class Ingredient {

    private String nomIngredient;
    private double preuIngredient;

    public Ingredient(String nomIngredient, double preuIngredient) {
        this.nomIngredient = nomIngredient;
        this.preuIngredient = preuIngredient;
    }

    public double getPreuIngredient() {
        return preuIngredient;
    }

    public String getNomIngredient() {
        return nomIngredient;
    }
}
