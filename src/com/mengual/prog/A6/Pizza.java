package com.mengual.prog.A6;

public class Pizza {

    private Ingredient[] ingredients = new Ingredient[5];
    private Masa masa;
    private float preu;

    public Pizza(Ingredient ingredients, Masa masa) {
        this.ingredients[0] = ingredients;
        this.masa = masa;
    }
    
    public Masa getMasa() {
        return masa;
    }

    public Ingredient[] getIngredients() {
        return ingredients;
    }

    public void addIngredient(Ingredient ingredient){

        for (int i = 0; i <= 5; i++){

            if (ingredients[i]==null){

                ingredients[i] = ingredient;

            }

        }

    }
}
