package com.mengual.prog.A6;

public class Masa {

    private String nomMasa;
    private double preuMasa;

    public Masa(String nomMasa, double preuMasa) {
        this.nomMasa = nomMasa;
        this.preuMasa = preuMasa;
    }

    public String getNomMasa() {
        return nomMasa;
    }

    public double getPreuMasa() {
        return preuMasa;
    }
}
