package com.mengual.prog.A13;

public class Activitat13 {

    private String cadena;

    public Activitat13(String cadena) {

        this.cadena = cadena;
    }

    public boolean codiPostal(){

        return cadena.matches("([0-4][0-9]|[5][0-2])[0-9]{3}");

    }

}
