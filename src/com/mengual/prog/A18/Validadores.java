package com.mengual.prog.A18;

public class Validadores {

    private String cadena;

    public Validadores(String cadena) {

        this.cadena = cadena;
    }

    public boolean codiPostal(){

        return cadena.matches("([0-4][0-9]|[5][0-2])[0-9]{3}");

    }

    public static boolean esValidoDni(String cadena){

        return cadena.matches("[0-9]{8}[A-H J-N P Q S T W-Z]");

    }

    public static boolean esValidMail(String cadena){

        return cadena.matches("^([a-zA-Z0-9][.]?)+[a-zA-Z0-9][@]([a-zA-Z0-9][\\-]*)+[a-zA-Z0-9][.][a-z]*");

    }

    public static boolean esValidaMac(String cadena){

        return cadena.matches("([0-9A-F]{2}:){5}[0-9A-F]{2}");

    }

    public static boolean esValidaIp(String cadena){

        return cadena.matches("(([1][0-9][0-9]|[2][0-5][0-5])[.]){3}([1][0-9][0-9]|[2][0-5][0-5])");

    }

}
