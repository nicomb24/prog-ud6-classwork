package com.mengual.prog.A2;

import java.util.Arrays;

public class Activitat2 {

    public static void main(String[] args){

        String[] array = {"0","1","2","3","4","5","6","7","8","9"};

        for (int i = 0; i < array.length; i += 2){
            System.out.print(array[i] + " ");
        }

        System.out.print("\n");

        for (int i = 1; i < array.length; i += 2){
            System.out.print(array[i] + " ");
        }

    }

}
