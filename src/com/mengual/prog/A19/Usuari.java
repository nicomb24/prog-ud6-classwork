package com.mengual.prog.A19;

import com.mengual.prog.A18.Validadores;

import java.util.Scanner;

public class Usuari {

    private String nom;

    private String cognom;

    private String dni;

    private String adreca;

    private String email;

    private Ordinador ordinador;

    public Usuari(String nom, String cognom, String dni, String adreca, String email) {
        this.nom = nom;
        this.cognom = cognom;
        this.dni = dni;
        this.adreca = adreca;
        this.email = email;
    }

    private void setNom() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Nom: \n");
        this.nom = scanner.nextLine();

    }

    private void setCognom(){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Cognom: \n");
        this.cognom = scanner.nextLine();

    }

    private void setAdreca(){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Adreça: \n");
        this.adreca = scanner.nextLine();

    }

    private void setDni(){

        Scanner scanner = new Scanner(System.in);

        String dni;

        do {

            System.out.print("DNI: \n");
            dni = scanner.nextLine();
            this.dni = dni;

            if (!Validadores.esValidoDni(dni)){
                Menu.showErrorMsg();
            }

        } while (!Validadores.esValidoDni(dni));

    }

    private void setEmail(){

        Scanner scanner = new Scanner(System.in);

        String email;

        do {

            System.out.print("Email: \n");
            email = scanner.nextLine();
            this.email = email;

            if (!Validadores.esValidMail(email)){
                Menu.showErrorMsg();
            }

        }while (!Validadores.esValidMail(email));

    }

    public void cambiarDades(){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Què vols modificar?\n " +
               "1. Nom\n" +
               "2. Cognom\n" +
               "3. DNI\n" +
               "4. Adreça\n" +
               "5. Email\n");

       int seleccio = scanner.nextInt();

       switch (seleccio){

           case 1:
               setNom();
               break;

           case 2:
               setCognom();
               break;

           case 3:
               setDni();
               break;

           case 4:
               setAdreca();
               break;

           case 5:
               setEmail();
               break;

       }

   }

   @Override
    public String toString(){

        return "Nom: " + nom +
                "\nCognoms: " + cognom +
                "\nAdreça: " + adreca +
                "\nDNI: " + dni +
                "\nEmail: " + email + "\n\n";

   }

}
