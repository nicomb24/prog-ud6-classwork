package com.mengual.prog.A19;

import com.mengual.prog.A18.Validadores;
import com.sun.tools.corba.se.idl.constExpr.Or;

import java.util.Scanner;

public class Menu {

    private String name;

    public Menu(String name) {
        this.name = name;
    }

    public void start(){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Benvingut al employee manager v.1.0\n +" +
                "Actualment no hi han dades seues, desitja introduirles(S/N)\n");

        if (scanner.next().equalsIgnoreCase("s")){

            showMenu();

        } else {
            System.out.print("Adios");
            return;
        }

    }

    public void showMenu(){

        Scanner scanner = new Scanner(System.in);

        Usuari usuari = null;

        usuari = introduirDades();

        int seleccio = 0;

        do {

            System.out.print("1. Visualitzar les dades\n" +
                    "2. Cambiar les dades\n" +
                    "3. Salir\n");

            seleccio = scanner.nextInt();

            if (seleccio == 1) {

                System.out.print(usuari);

            } else if (seleccio == 2){

                usuari.cambiarDades();

            }

        } while (seleccio!=3);

    }

    public Usuari introduirDades(){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Employee manager v.1.0 (Introducció de dades)\n");

        System.out.print("Nom: \n");
        String nom = scanner.nextLine();

        System.out.print("Cognoms: \n");
        String cognoms = scanner.nextLine();

        String dni;

        do {

            System.out.print("DNI: \n");
            dni = scanner.nextLine();

            if (!Validadores.esValidoDni(dni)){
                showErrorMsg();
            }

        } while (!Validadores.esValidoDni(dni));

        System.out.print("Adreça: \n");
        String adreca = scanner.nextLine();

        String email;

        do {

            System.out.print("Email: \n");
            email = scanner.nextLine();

            if (!Validadores.esValidMail(email)){
                showErrorMsg();
            }

        }while (!Validadores.esValidMail(email));


        Usuari usuario = new Usuari(nom, cognoms, dni, adreca, email);

        return usuario;

    }

    public static void showErrorMsg(){

        System.out.print("La dada és incorrecta, modifica-la\n\n");

    }

}
