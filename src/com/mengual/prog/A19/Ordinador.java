package com.mengual.prog.A19;

import com.mengual.prog.A18.Validadores;

import java.util.Scanner;

public class Ordinador {

    private String marca;

    private String model;

    private String ip;

    private String mac;


    public Ordinador(String marca, String model, String ip, String mac) {
        this.marca = marca;
        this.model = model;
        this.ip = ip;
        this.mac = mac;
    }

    public static Ordinador introduirDadesOrdinador(){

        //Ordinador ordinadorNull = new Ordinador();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Marca: \n");
        String marca = scanner.nextLine();

        System.out.print("Model: \n");
        String model = scanner.nextLine();

        System.out.print("IP: \n");
        String ip = scanner.nextLine();

        if (Validadores.esValidaIp(ip)) {

            System.out.print("MAC: \n");
            String mac = scanner.nextLine();

            if (Validadores.esValidaMac(mac)) {

                Ordinador ordinador = new Ordinador(marca,model,ip,mac);

                return ordinador;
            } else {


                return null;

            }
        } else {


            return null;

        }

    }
}
